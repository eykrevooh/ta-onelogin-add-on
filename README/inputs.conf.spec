[onelogin_input://<name>]
python.version = python3
global_account =
start_time = The date (UTC in \"YYYY-MM-DDThh:mm:ssZ\" format) from when to start collecting the data. Default value taken will be start of epoch time.
log_type = OneLogin endpoint log type (ie. Events, Users, Groups, Roles, Apps, Connectors)
