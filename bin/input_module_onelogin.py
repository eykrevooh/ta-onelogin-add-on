from onelogin_validations import *
from onelogin_collector import OneLoginCollector


def validate_input(helper, definition):
    '''
    Validates the input parameters and provides error to user on UI if the validation fails.
    :param helper: object of BaseModInput class
    :param definition: object containing input parameters
    '''
    app_name = helper.get_app_name()
    name = definition.metadata['name']
    global_account_name = definition.parameters.get('global_account')
    validate_interval(helper, definition.parameters.get("interval"))
    validate_start_time(helper, definition.parameters.get("start_time"))

def collect_events(helper, ew):
    pass
    '''
    Collect data by making REST call to portal.bitglass.com.
    :param helper: object of BaseModInput class
    :param ew: object of EventWriter class
    '''

    ingester = OneLoginCollector(helper, ew)
    ingester.collect_events()
