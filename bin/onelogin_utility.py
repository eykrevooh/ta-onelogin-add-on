import io
import os
import re
import json
import requests
import datetime
import six.moves.configparser
import time
import calendar
from math import ceil

import splunk.rest as rest
from splunk.clilib.bundle_paths import make_splunkhome_path

def get_auth_token(dest_url, client_id, client_secret):
    headers={"Content-Type":"application/json"}
    body={"grant_type":"client_credentials"}
    resp = requests.post(dest_url, auth=(client_id, client_secret), headers=headers, json=body)
    status_code = resp.status_code
    if status_code==200:
      jsonified=resp.json()
      auth_token=jsonified["access_token"]
    else:
      self.helper.log_error("Unable to Authenticate Error: {} {}".format(status_code, resp.content))
      auth_token=None
    return auth_token

def get_configuration(my_app, file, folder="local"):
    conf_parser = six.moves.configparser.ConfigParser()
    conf = os.path.join(make_splunkhome_path(
        ["etc", "apps", my_app, folder, file]))
    stanzas = []
    if os.path.isfile(conf):
        with io.open(conf, 'r', encoding='utf_8_sig') as conffp:
            conf_parser.readfp(conffp)
        stanzas = conf_parser.sections()
    return conf_parser, stanzas

def get_app_version(my_app):
    config, stanza = get_configuration(my_app, 'app.conf', folder='default')
    return '{}-b{}'.format(
        config.get('launcher', 'version'),
        config.get('install', 'build')
    )

def get_account_data(global_account, my_app):
    account_config, account_stanzas = get_configuration(
        my_app, "ta_onelogin_account.conf")
    account_dict = {}

    for stanza in account_stanzas:
        if str(stanza) == global_account:
            account_dict["address"] = account_config.get(stanza, 'address')
            account_dict["verify_ssl"] = account_config.get(
                stanza, 'verify_ssl')
            account_dict["onelogin_account_type"] = account_config.get(
                stanza, 'onelogin_account_type')
            set_proxy_attributes(account_config, account_dict, stanza)

    return account_config, account_dict

def get_kvstore_status(session_key):
    _, content = rest.simpleRequest("/services/kvstore/status", sessionKey=session_key,
                                    method="GET", getargs={"output_mode": "json"}, raiseAllErrors=True)
    data = json.loads(content)['entry']
    return data[0]["content"]["current"].get("status")


def check_kvstore_status(session_key):
    status = get_kvstore_status(session_key)
    count = 0
    while status != 'ready':
        if status == 'starting':
            count += 1
            if count < 3:
                time.sleep(30)
                status = get_kvstore_status(session_key)
                continue
        raise Exception(
            "KV store is not in ready state. Current state: " + str(status))
