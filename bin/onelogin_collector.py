# python imports
import time
import json
import arrow
import onelogin_utility as utility
import base64

import datetime
import requests
import sys

import splunk.rest as rest

TIME_PATTERN = "%Y-%m-%dT%H:%M:%SZ"

URL_APPENDERS={"events":{"url":"/api/1/events", "checkpoint":True}, "event_types":{"url":"/api/1/events/types", "checkpoint":False}, "users":{"url":"/api/1/users", "checkpoint":False}, "groups":{"url":"/api/1/groups", "checkpoint":False}, "roles":{"url":"/api/1/roles", "checkpoint":False}, "apps":{"url":"/api/1/apps", "checkpoint":False}, "auth":{"url":"/auth/oauth2/v2/token", "checkpoint":False}}

class OneLoginCollector(object):
    """Base class for the bitglass collector.
    This class sets the common input params.
    Along with that it also waits on kvstore to be in ready state,
    because the add-on uses it for maintaining the time checkpoints.
    """

    def __init__(self, helper, ew):
        """
        Args:
            helper (object): object of BaseModInput class
            ew (object): object of event writer class
        """
        self.helper = helper
        self.event_writer = ew
        self.app_name = helper.get_app_name()
        self.build = utility.get_app_version(self.app_name)
        self._wait_for_kvstore()
        self._set_input_data()
        self.current_time = int(time.time())

    def _set_input_data(self):
        """Set common imput form fields for data collection.
        """
        self.input_name = self.helper.get_input_stanza_names()
        self.interval = self.helper.get_arg("interval")
        self.index = self.helper.get_arg("index")
        self._account = self.helper.get_arg("global_account")
        self.log_type = self.helper.get_arg("log_type")
        self.client_id = self._account["client_id"]
        self.client_secret = self._account["client_secret"]
        self.start_time = self.helper.get_arg("start_time") if self.helper.get_arg(
            "start_time") else "1970-01-01T00:00:00Z"
        self.sourcetype ="onelogin:{}".format(self.log_type)


    def _wait_for_kvstore(self):
        """Wait for KV store to initialize.
        KV store is used for maintaining time checkpoints for all the different exports.

        Raises:
            Exception: when kv store is not in ready state
        """
        def get_status():
            _, content = rest.simpleRequest("/services/kvstore/status",
                                            sessionKey=session_key,
                                            method="GET",
                                            getargs={"output_mode": "json"},
                                            raiseAllErrors=True)
            data = json.loads(content)["entry"]
            return data[0]["content"]["current"].get("status")

        session_key = self.helper.context_meta["session_key"]
        counter = 0
        status = get_status()
        self.helper.log_debug("KV Status: {}".format(status))
        while status != "ready":
            if status == "starting":
                counter += 1
                if counter < 3:
                    time.sleep(30)
                    status = get_status()
                    continue
            self.helper.log_error("KV store is not in ready state. Current state: " + str(status))
            raise Exception(
                "KV store is not in ready state. Current state: " + str(status))

    def get_checkpoint(self):
        """Return checkpoint based on export type and time filter field.
        """
        check_point_name = self.input_name
        self.helper.log_debug(
            "Checkpoint name is {}".format(check_point_name))

        state = self.helper.get_check_point(check_point_name)
        self.helper.log_debug(
            "Checkpoint state returned is {}".format(state))

        # in case if checkpoint is not found state value will be None,
        # so we are setting it to empty dict
        if not isinstance(state, dict):
            state = {}
        return state

    def save_checkpoint(self, time):
        """Save checkpoint state with name formed from input name.
        """
        self.helper.save_check_point(self.input_name, time)
        self.helper.log_debug(
            "Check point state saved is " + str(time))

    def _get_logs(self, dest_url, auth_token, start_date=None, next_page_token=None):
        """Request Bitglass API for logs of specified log_type

        Args:
            start_date (str): str of most earliest time to be retrieved format %Y-%m-%dT%H:%M:%SZ
            next_page_token (str): str to use for requesting next page of results

        Returns:
           events (dict): {next_page_token:"New Next Page Value", logs:[List of logs]}
        """
        result={"status":"complete", "next_page_token":"", "last_event":""}
        last_event_time=""
        if start_date and next_page_token:
          parameters={"since":start_date, "after_cursor":next_page_token}
        elif start_date and not next_page_token:
          parameters={"since":start_date}
        elif next_page_token and not start_date:
          parameters={"after_cursor":next_page_token}
        else:
          parameters={}
        try:
          headers={"Authorization":"bearer:{}".format(auth_token)}
          response = requests.get(dest_url, headers=headers, params=parameters)
        except Exception as e:
          self.helper.log_error("Broken response " + str(e))
        try:
            status_code = response.status_code
            jsonified=response.json()
            if status_code==400:
              self.helper.log_error("Error 400: {}".format(jsonified))
            elif status_code==401:
              self.helper.log_error("Error 401 Invalid Credentials or Token")
            elif status_code==200:
              for event in jsonified["data"]:
                self.write_event(event, self.sourcetype)
              if "created_at" in jsonified["data"][len(jsonified["data"])-1]:
                last_event_time=jsonified["data"][len(jsonified["data"])-1]["created_at"]
              else:
                ##No Last event time
                pass
              if "pagination" in jsonified:
                self.helper.log_debug("Multipage Result: {}".format(jsonified["pagination"]["after_cursor"]))
                result={"status":"incomplete","next_page_token":jsonified["pagination"]["after_cursor"],"last_event":last_event_time}
              else:
                self.helper.log_debug("Single Page Response, collection complete")
                result={"status":"complete","next_page_token":"","last_event":last_event_time}
            else:
              self.helper.log_error("Unknown Status code {} returned by {}: ".format(str(status_code), dest_url))
        except Exception as e:
            self.helper.log_error("Broken property retrival " + str(e))
        return result

    def _event_transformer(self, event):
        """Transforms, modifies and updates the received json event.

        Args:
            event (dict): access, admin, cloudaudit or cloudsummary event

        Returns:
            dict, int: transformed event and epoch time on which to index the event
        """
        try:
          if URL_APPENDERS[self.log_type]['checkpoint']==True:
            event["address"] = self._account["address"]
            event_time = arrow.get(event["created_at"]).timestamp
          else:
            event_time = arrow.utcnow().timestamp
        except Exception as e:
          event_time = arrow.utcnow().timestamp
          self.helper.log_error("Transfrom Error: {}".format(str(e)))

        return event, event_time

    def write_event(self, event, sourcetype):
        """Index the event into the splunk into given sourcetype.
        Events are transformed first before ingesting into the splunk.

        Args:
            event (dict): event to index
            sourcetype (str): sourcetype in which to index the events
        """
        try:
            event, event_time = self._event_transformer(event)
            parsed_event = json.dumps(event)
            self.helper.log_debug("Parsed Event Length: " + str(len(parsed_event)))
            self.helper.log_debug("Index: {} \n Sourcetype: {} \n Time: {}".format(self.index, sourcetype, event_time))
            #self.helper.log_debug("Parsed Event: " + parsed_event)
            event_obj = self.helper.new_event(
                data=parsed_event, time=event_time, index=self.index, sourcetype=sourcetype, unbroken=True)
            self.event_writer.write_event(event_obj)
        except Exception as e:
            self.helper.log_error("Disk Write Error: {}".format(e))

    def collect_events(self):
      """Collect OneLogin logs
         Loops until either error thrown by OneLogin API or till a blank page is returned
      """
      ##TODO Clean up this function, checkpointing can be a clean function
      ##TODO Abstract ingestion loop into Checkpointed and non-checkpointed functions
      self.helper.log_info("OneLogin data collection started for input: {}".format(self.input_name))
      ##Initialize Variables
      base_url=self._account['address']
      auth_token = utility.get_auth_token(base_url+URL_APPENDERS['auth']['url'], self.client_id, self.client_secret)
      complete=False
      next_page_token=None
      checkpoint=""
      backup_time=""
      ##Very dumb iterator
      ###OneLogin does not seem to enforce limits, so we need to occasionally backup the last ingest time
      iterator=0

      ##Get the log type
      url=base_url+URL_APPENDERS[self.log_type]['url']
      if URL_APPENDERS[self.log_type]['checkpoint']==True:
        try:
          self.start_time=self.get_checkpoint()['time']
          self.helper.log_debug("Got OneLogin {} checkpoint: {}".format(self.input_name, self.start_time))
        except:
          self.helper.log_debug("No Checkpoint set for {}, using input default {}".format(self.input_name, self.start_time))
      try:
          while complete==False:
              self.helper.log_debug("Bitglss next_page_token: {}".format(next_page_token))
              if URL_APPENDERS[self.log_type]['checkpoint']==True:
                if next_page_token:
                  self.helper.log_debug("Fetching logs with next_page_token: {}".format(next_page_token))
                  result = self._get_logs(url, auth_token, start_date=self.start_time, next_page_token=next_page_token)
                else:
                  self.helper.log_debug("Fetching logs with start_date: {}".format(self.start_time))
                  result = self._get_logs(url, auth_token, start_date=self.start_time)
              else:
                #Non-checkpointed
                if next_page_token:
                  ##Non-checkpointed log, moving through pages
                  self.helper.log_debug("Fetching logs with start_time")
                  result = self._get_logs(url, auth_token, next_page_token=next_page_token)
                else:
                  ##Non-checkpointed log, get all of the data
                  self.helper.log_debug("Fetching logs with start_time")
                  result = self._get_logs(url, auth_token)
              if (result["status"]=="complete" or result["next_page_token"]==None):
                if URL_APPENDERS[self.log_type]['checkpoint']==True:
                  self.helper.log_info("Bitglass Collection Complete {}, saving checkpoint".format(self.input_name))
                  if result["last_event"]:
                    self.save_checkpoint({"time":result["last_event"]})
                    self.helper.log_info("OneLogin saving final checkpoint: {}".format(result["last_event"]))
                  else:
                    ##Error with ingestor, reverting to last known log time, will result in duplicate events
                    self.save_checkpoint({"time":backup_time})
                    self.helper.log_error("OneLogin saving final backup checkpoint: {}".format(backup_time))
                else:
                  #Non-checkpoint event, do nothing
                  pass
                complete=True
              else:
                self.helper.log_debug("OneLogin Collection Incomplete, next_page: {}".format(result["next_page_token"]))
                backup_time=result["last_event"]
                self.helper.log_debug("OneLogin backup_time set: {}".format(backup_time))
                next_page_token=result["next_page_token"]
                if (iterator==250 and URL_APPENDERS[self.log_type]['checkpoint']==True):
                  ##Every 251 calls backup the checkpoint time
                  iterator=0
                  if result["last_event"]:
                    self.helper.log_info("OneLogin saving temp checkpoint: {}".format(result["last_event"]))
                    self.save_checkpoint({"time":result["last_event"]})
                  else:
                    ##No time on last event
                    pass
                else:
                  iterator+=1

      except Exception as e:
          self.helper.log_error("OneLogin exports error occured during get log collection: {}".format(str(e)))
          #log that we completed with everything
      self.helper.log_info("OneLogin data collection completed for input: {}".format(self.input_name))
